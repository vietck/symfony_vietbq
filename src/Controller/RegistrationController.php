<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserRepository $userRepository,
                                UserPasswordEncoderInterface $passwordEncoder,
                                EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('email', EmailType::class, [
                'required' => true,
                'attr' => ['maxlength' => 255],
            ])
            ->add('username', TextType::class, [
                'required' => true,
                'attr' => ['minlength' => 5, 'maxlength' => 60],
            ])
            ->add('password', PasswordType::class, [
                'required' => true,
                'attr' => ['minlength' => 8, 'maxlength' => 255],
            ])
            ->add('register', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $user_1 = $this->userRepository->findOneBy([
                'username' => $user->getUsername(),
            ]);

            if (!is_null($user_1)) {
                $this->addFlash(
                    'danger',
                    'Username already exists!'
                );
            }

            $user_2 = $this->userRepository->findOneBy([
                'email' => $user->getEmail(),
            ]);

            if (!is_null($user_2)) {
                $this->addFlash(
                    'danger',
                    'Email already exists!'
                );
            }

            if (!is_null($user_1) || !is_null($user_2)) {
                return $this->redirectToRoute('register');
            }

            $user->setPassword(
                $this->passwordEncoder->encodePassword($user, $user->getPassword())
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash(
                'success',
                'You have successfully registered! Please login!'
            );

            return $this->redirectToRoute('login');
        }

        return $this->render('registration/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
