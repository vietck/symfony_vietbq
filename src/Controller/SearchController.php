<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function renderSearchBar(Request $request)
    {
        $formSearch = $this->createFormBuilder()
            ->add('keyWord', TextType::class)
            ->add('search', SubmitType::class)
            ->getForm();

        $formSearch->handleRequest($request);
        if ($formSearch->isSubmitted() && $formSearch->isValid()) {
            $data = $formSearch->getData();
            $articleRepo = $this->getDoctrine()->getRepository(Article::class);
            $articles = $articleRepo->search($data['keyWord']);

            return $this->render('article/index.html.twig', [
                'articles' => $articles,
            ]);
        }

        return $this->render('search/search_bar.html.twig', [
            'form' => $formSearch->createView(),
        ]);
    }
}
