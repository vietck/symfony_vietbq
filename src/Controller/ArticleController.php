<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ArticleController extends AbstractController
{

    /**
     * @Route("/", name="articles")
     */
    public function index()
    {
        $repository = $this->getDoctrine()
            ->getRepository(Article::class);
        $articles = $repository->findAll();

        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/my-articles", name="my_articles")
     */
    public function myList(UserInterface $user = null)
    {
        $articles = null;
        if ($user != null)
            $articles = $user->getArticles();

        return $this->render('article/my_list.html.twig', [
            'username' => $user->getUsername(),
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/article/add", name="article_add")
     */
    public function add(Request $request, UserInterface $user = null)
    {
        $article = new Article();

        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [
                'required' => true,
                'attr' => ['minlength' => 5, 'maxlength' => 255],
            ])
            ->add('content', TextareaType::class, [
                'required' => true,
                'attr' => ['minlength' => 20, 'maxlength' => 2000],
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary float-right',
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $article = $form->getData();
            $article->setUser($user);
            $article->setCreateAt(new \DateTime());
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'The article was saved successfully!'
            );

            return $this->redirectToRoute('my_articles');
        }

        return $this->render('article/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}", name="article_show")
     */
    public function show($id)
    {
        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->find($id);

        if (!$article) {
            $this->addFlash(
                'danger',
                'No product found for id ' . $id
            );

            return $this->redirectToRoute('articles');
        }

        return $this->render('article/detail.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/update/{id}", name="article_update")
     */
    public function update(Request $request, $id, UserInterface $user = null)
    {
        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->find($id);

        if (!$article) {
            $this->addFlash(
                'danger',
                'No product found for id ' . $id
            );

            return $this->redirectToRoute('articles');
        }

        if ($article->getUser()->getId() != $user->getId()) {
            $this->addFlash(
                'danger',
                'You do not have permission to perform this function!'
            );

            return $this->redirectToRoute('articles');
        }

        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [
                'required' => true,
                'attr' => ['minlength' => 5, 'maxlength' => 255],
            ])
            ->add('content', TextareaType::class, [
                'required' => true,
                'attr' => ['minlength' => 20, 'maxlength' => 2000],
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary float-right',
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $article = $form->getData();
            $article->setCreateAt(new \DateTime());
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'The article was updated successfully!'
            );

            return $this->redirectToRoute('my_articles');
        }

        return $this->render('article/add.html.twig', [
            'form' => $form->createView(),
            'update' => null,
        ]);
    }

    /**
     * @Route("/article/delete/{id}", name="article_delete")
     */
    public function delete($id, UserInterface $user = null)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Article::class)->find($id);

        if (!$article) {
            $this->addFlash(
                'danger',
                'No product found for id ' . $id
            );

            return $this->redirectToRoute('articles');
        }

        if ($article->getUser()->getId() != $user->getId()) {
            $this->addFlash(
                'danger',
                'You do not have permission to perform this function!'
            );

            return $this->redirectToRoute('articles');
        }

        $entityManager->remove($article);
        $entityManager->flush();

        $this->addFlash(
            'danger',
            'The article was deleted successfully!'
        );

        return $this->redirectToRoute('my_articles');
    }
}
